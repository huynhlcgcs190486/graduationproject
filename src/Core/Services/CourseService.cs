﻿using Ardalis.Result;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Class;
using Core.Entities;
using Core.Interfaces;
using Core.Specifications;
using SharedKernel.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.Endpoints.Courses;

namespace Core.Services
{
    public class CourseService : ICourseService
    {
        private readonly IRepository _repository;
        private readonly IInstructorRepository _instructorRepository;
        private readonly Cloudinary _cloudinary;

        public CourseService(IRepository repository, Cloudinary cloudinary, IInstructorRepository instructorRepository)
        {
            _repository = repository;
            _cloudinary = cloudinary;
            _instructorRepository = instructorRepository;
        }

        public async Task<Course> GetDetailCourse(Guid id)
        {
            var incompleteSpec = new GetDetailCourse(id);

            try
            {
                var items = await _repository.ListAsync(incompleteSpec);

                return items.First();
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<Course>.Error(new[] { ex.Message });
            }
        }
        
        public async Task<List<Course>> GetAllCourse<Course>()
        {
            var incomplete = new GetAllCourse();
            try
            {
                var courses = await _repository.ListAsync(incomplete);

                return new List<Course>((IEnumerable<Course>)courses);
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<List<Course>>.Error(new[] { ex.Message });
            }
        }
        
        public async Task<List<Course>> SearchCourse<Course>()
        {
            var incompleteSpec = new SearchCourse();
            try
            {
                var items = await _repository.ListAsync(incompleteSpec);

                return new List<Course>((IEnumerable<Course>)items);
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<List<Course>>.Error(new[] { ex.Message });
            }
        }
        
        public async Task<List<Entities.Course>> FilterCourse<Course>(Guid[]? category, int? min, int? max)
        {
            var incompleteSpec = new SearchCourse();
            try
            {
                var result = new List<Entities.Course>();
                var items = await _repository.ListAsync(incompleteSpec);
                if (category != null && min != null && max != null)
                {
                    foreach (var i in category)
                    {
                        var arr = items.FindAll(item => item.SubCategoryId == i && item.OriginPrice >= min && item.OriginPrice <= max);
                        arr.ForEach(item => result.Add(item));
                    }
                }
                
                if (category != null && min == null && max == null)
                {
                    foreach (var i in category)
                    {
                        var arr = items.FindAll(item => item.SubCategoryId == i);
                        arr.ForEach(item => result.Add(item));
                    }
                }
                
                if (category == null && min != null && max != null)
                {
                    result = items.FindAll(item => item.OriginPrice >= min && item.OriginPrice <= max);
                }
                
                if (category == null && min == null && max == null)
                {
                    result = items; ;
                }

                return result;
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<List<Entities.Course>>.Error(new[] { ex.Message });
            }
        }

        public async Task<List<Course>> GetApproveCourse<Course>()
        {
            var incompleteSpec = new GetApproveItem();
            try
            {
                var items = await _repository.ListAsync(incompleteSpec);

                return new List<Course>((IEnumerable<Course>)items);
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<List<Course>>.Error(new[] { ex.Message });
            }
        }

        public async Task<List<Course>> GetRejectedCourse<Course>()
        {
            var incompleteSpec = new GetRejectedItem();
            try
            {
                var items = await _repository.ListAsync(incompleteSpec);

                return new List<Course>((IEnumerable<Course>)items);
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<List<Course>>.Error(new[] { ex.Message });
            }
        }

        public async Task<List<Course>> GetBlockCourse<Course>()
        {
            var incompleteSpec = new GetBlockItem();
            try
            {
                var items = await _repository.ListAsync(incompleteSpec);

                return new List<Course>((IEnumerable<Course>)items);
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<List<Course>>.Error(new[] { ex.Message });
            }
        }

        public async Task<List<Course>> GetActiveCourse<Course>()
        {
            var incompleteSpec = new GetActiveItem();
            try
            {
                var items = await _repository.ListAsync(incompleteSpec);

                return new List<Course>((IEnumerable<Course>)items);
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<List<Course>>.Error(new[] { ex.Message });
            }
        }


        public async Task<List<Course>> GetActiveCourseStudent<Course>()
        {
            var incompleteSpec = new GetActiveItemStudent();
            try
            {
                var items = await _repository.ListAsync(incompleteSpec);

                return new List<Course>((IEnumerable<Course>)items);
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<List<Course>>.Error(new[] { ex.Message });
            }
        }

        /// <summary>
        /// Update Course's Status to Waiting for approval
        /// </summary>
        /// <returns>Value of updated result</returns>
        public async Task<bool> RejectedCourse(Guid id)
        {
            try
            {
                //find Course by Id
                Course course = await _repository.GetByIdAsync<Course>(id);

                //Updated Course's Status
                course.IsRejected = true;
                course.Status = "Reject";

                //Set updated date
                course.UpdateAt = DateTime.Now;

                //Update and SaveChange
                if (await _repository.UpdateAsync<Course>(course) == 1)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<bool>.Error(new[] { ex.Message });
            }
        }
        
        /// <summary>
        /// Update Course's Feature
        /// </summary>
        /// <returns>Value of updated result</returns>
        public async Task<bool> Feature(Guid id)
        {
            try
            {
                //find Course by Id
                Course course = await _repository.GetByIdAsync<Course>(id);

                //Check status must be "Active"
                if (course.Status.Trim().ToLower().Equals("active"))
                    return false;

                //Updated Course's feature
                course.IsFeatured = !course.IsFeatured;

                //Set updated date
                course.UpdateAt = DateTime.Now;

                //Update and SaveChange
                if (await _repository.UpdateAsync<Course>(course) == 1)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<bool>.Error(new[] { ex.Message });
            }
        }
        
        /// <summary>
        /// Update Course's IsBestSeller to true or false
        /// </summary>
        /// <returns>Value of updated result</returns>
        public async Task<bool> BestSeller(Guid id)
        {
            try
            {
                //find Course by Id
                Course course = await _repository.GetByIdAsync<Course>(id);

                //Check status must be "Active"
                if (course.Status.Trim().ToLower().Equals("active"))
                    return false;

                //Updated Course's IsBestSeller
                course.IsBestSeller = !course.IsBestSeller;

                //Set updated date
                course.UpdateAt = DateTime.Now;

                //Update and SaveChange
                if (await _repository.UpdateAsync<Course>(course) == 1)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<bool>.Error(new[] { ex.Message });
            }
        }
        
        /// <summary>
        /// Update Course's Status to Blocked or last status
        /// </summary>
        /// <returns>Value of updated result</returns>
        public async Task<bool> BlockedCourse(Guid id)
        {
            try
            {
                //find Course by Id
                Course course = await _repository.GetByIdAsync<Course>(id);

                course.IsBlocked = true;

                course.Status = "Block";

                //Set updated date
                course.UpdateAt = DateTime.Now;

                //Update and SaveChange
                if (await _repository.UpdateAsync<Course>(course) == 1)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<bool>.Error(new[] { ex.Message });
            }
        }
        
        /// <summary>
        /// Update Course's Status to Active
        /// </summary>
        /// <returns>Value of updated result</returns>
        public async Task<bool> ApprovedCourse(Guid id)
        {
            try
            {
                //find Course by Id
                Course course = await  _repository.GetByIdAsync<Course>(id);

                //Check status must be "Waiting for approved"
                /*if (course.Status.Trim().ToLower().Equals("waiting for approved"))
                    return false;*/

                //Updated Course's Status
                course.IsBlocked = false;
                course.Status = "Active";

                //Set updated date
                course.UpdateAt = DateTime.Now;

                //Update and SaveChange
                if (await _repository.UpdateAsync<Course>(course) == 1)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<bool>.Error(new[] { ex.Message });
            }
        }

        public async Task<Course> CreateCourseWithImg(CourseMain request, Guid id)
        {
            var item = new Course
            {
                UserId =id,
                Title = request.Title,
                Description = request.Description,
                LanguageId = request.LanguageId,
                SubCategoryId = request.SubCategoryId,
                //ImageUrl = await UploadImage(request.BackgroupCourse),
                TrailerUrl = await UploadVideo(request.BackgroupCourse),
                CreateAt = DateTime.Now,
                UpdateAt = DateTime.Now,
                Status = "Waiting for approve",
                OriginPrice = request.Price

            };

            if(item.TrailerUrl == "")
            {
                return null;
            }

            return await _repository.AddAsync<Course>(item);
        }

        /// <summary>
        /// Get discount of promotion
        /// </summary>
        /// <param name="promotionId">Id of promotion</param>
        /// <returns>Value of discount</returns>
        public async Task<decimal> GetDiscount(Guid promotionId)
        {
            Promotion promotion = await _repository.GetByIdAsync<Promotion>(promotionId);

            return promotion.DiscountPercent;
        }

        /// <summary>
        /// Check course must be owned user
        /// </summary>
        /// <param name="courseId">course Id</param>
        /// <param name="userId">user Id</param>
        /// <returns>true if it's already exits</returns>
        public async Task<bool> CheckCourseOfUser(Guid courseId, Guid userId)
        {
            Course course = await _repository.GetByIdAsync<Course>(courseId);

            if (course?.UserId == userId)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Upload to Cloudinary for image and trailer video
        /// </summary>
        /// <param name="courseId">course id</param>
        /// <param name="image">image file</param>
        /// <param name="trailer">image file</param>
        /// <returns>true if it's success</returns>
        public async Task<bool> UpdateView(Guid courseId, IFormFile image, IFormFile trailer)
        {
            //find Course by Id
            Course course = await _repository.GetByIdAsync<Course>(courseId);

            //set url for uploaded file
            course.ImageUrl = await UploadFile(image);
            course.TrailerUrl = await UploadFile(trailer);

            course.UpdateAt = DateTime.Now;

            //Update and SaveChange
            if (await _repository.UpdateAsync<Course>(course) == 1)
            {
                return true;
            }

            return false;
        }

        private async Task<string> UploadFile(IFormFile file)
        {
            //kiểm tra có ảnh hay không
            if (file != null)
            {
                #region Upload image to Cloudinary
                var results = new List<Dictionary<string, string>>();

                IFormatProvider provider = CultureInfo.CreateSpecificCulture("en-US");
                var result = await _cloudinary.UploadLargeAsync(new VideoUploadParams
                {
                    File = new FileDescription(file.FileName,
                        file.OpenReadStream()),
                    Tags = "backend_PhotoAlbum",
                    PublicId = "samples/Course/Video/" + file.FileName,
                    EagerTransforms = new List<Transformation>()
                      {
                        new EagerTransformation().Width(300).Height(300).Crop("pad").AudioCodec("none")
                      },
                        EagerAsync = true
                }).ConfigureAwait(false);

                //nếu upload thất bại
                if (result.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    return "";
                }

                var imageProperties = new Dictionary<string, string>();
                foreach (var token in result.JsonObj.Children())
                {
                    //tìm thuộc tính url để gán cho post
                    if (token is JProperty prop)
                    {
                        if (prop.Name.Equals("url"))
                            return prop.Value.ToString();
                    }
                }

                results.Add(imageProperties);
                #endregion
            }

            return "";
        }

        private async Task<string> UploadVideo(IFormFile file)
        {
            //kiểm tra có ảnh hay không
            if (file != null)
            {
                #region Upload image to Cloudinary
                var results = new List<Dictionary<string, string>>();
                IFormatProvider provider = CultureInfo.CreateSpecificCulture("en-US");
                var result = await _cloudinary.UploadAsync(new VideoUploadParams
                {
                    File = new FileDescription(file.FileName,
                        file.OpenReadStream()),
                    Tags = "backend_PhotoAlbum",
                    PublicId = "samples/Course/image/" + file.FileName
                }).ConfigureAwait(false);

                //nếu upload thất bại
                if (result.StatusCode == System.Net.HttpStatusCode.BadRequest)
                {
                    return "";
                }

                var imageProperties = new Dictionary<string, string>();
                foreach (var token in result.JsonObj.Children())
                {
                    //tìm thuộc tính url để gán cho post
                    if (token is JProperty prop)
                    {
                        if (prop.Name.Equals("url"))
                            return prop.Value.ToString();
                    }
                }

                results.Add(imageProperties);
                
                #endregion
            }

            return "";
        }

        /// <summary>
        /// Create by:
        ///         Lesson table
        ///         Section table
        /// </summary>
        /// <returns>true if it's success</returns>
        public async Task<bool> CreateCourseEnrollment(Guid courseId, Guid userId)
        {
            try
            {
                Enrollment enrollment = new Enrollment();
                enrollment.CourseId = courseId;
                enrollment.UserId = userId;
                enrollment.IsDeleted = false;
                enrollment.CreateAt = DateTime.Now;
                enrollment.UpdateAt = DateTime.Now;

                //Add new Section and Lesson
                await _repository.AddAsync<Enrollment>(enrollment);
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
            
        }

        public async Task<bool> CreateComment(CommentsRequest discussion)
        {
            try
            {
                Discussion comment = new Discussion();
                comment.CourseId = discussion.courseId;
                comment.UserId = discussion.userId;
                comment.Comment = discussion.comment;
                comment.SenderId = discussion.userId;
                comment.parentId = discussion.parentId;
                comment.IsDeleted = false;
                comment.CreateAt = DateTime.Now;
                comment.UpdateAt = DateTime.Now;

                //Add new Section and Lesson
                await _repository.AddAsync<Discussion>(comment);
                return true;
            }
            catch (Exception e)
            {
                return false;
            }

        }

        public async Task<Guid> CreateCourseSection( Section section)
        {
            return (await _repository.AddAsync<Section>(section)).Id;

        }

        /// <summary>
        /// Get total time of total Lessons
        /// </summary>
        /// <param name="lessons"></param>
        /// <returns>value of Total time</returns>
        private int GetTotalTime(LessonContent[] lessons)
        {
            int result = 0;

            foreach (LessonContent lesson in lessons)
            {
                result += (int)lesson.Duration;
            }

            return result;
        }



        // add one lesson
        public async Task<bool> AddLesson(LessonContent lessonContent)
        {
            
            try
            {
                Lesson lesson = new Lesson();

                //Set value for element in Lesson list
                lesson.SectionId = lessonContent.Id;
                lesson.Title = lessonContent.LessonTitle;
                lesson.CreateAt = DateTime.Now;
                lesson.VideoUrl = await UploadVideo(lessonContent.VideoFile);
                lesson.Duration = lessonContent.Duration;
                await _repository.AddAsync<Lesson>(lesson);
                return true;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Delete Section by id
        /// </summary>
        /// <param name="sectionId">section id</param>
        /// <returns>true if it's success</returns>
        public async Task<bool> DeleteSection(Guid sectionId)
        {
            try
            {
                await _repository.DeleteListAsync<Lesson>(item => item.SectionId == sectionId);

                await _repository.DeleteByIdAsync<Section>(sectionId);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> DeleteLeture(Guid Id)
        {
            try
            {
                await _repository.DeleteByIdAsync<Lesson>(Id);

                return true;
            }
            catch
            {
                return false;
            }
        }


        public async Task<bool> DeleteCourse(Guid Id)
        {
            var incompleteSpec = new GetSectionByCourseId(Id);
            try
            {
                var section = await _repository.ListAsync<Section>(incompleteSpec);
                foreach (var item in section)
                {
                    await _repository.DeleteListAsync<Lesson>(x => x.SectionId == item.Id);

                    await _repository.DeleteByIdAsync<Section>(item.Id);
                }
                await _repository.DeleteByIdAsync<Course>(Id);
                return true;
            }
            catch
            {
                return false;
            }
        }


        public async Task<bool> DeleteQuestion(Guid Id)
        {
            var incompleteSpec = new GetDetailQuestionByID(Id);
            try
            {
                var question = await _repository.ListAsync<QuizzQuestion>(incompleteSpec);
                foreach (var item in question)
                {
                    await _repository.DeleteListAsync<QuizzAnswer>(x => x.QuestionId == item.Id);
                }
                await _repository.DeleteByIdAsync<QuizzQuestion>(Id);
                return true;
            }
            catch
            {
                return false;
            }
        }


        public async Task<bool> DeleteQuizz(Guid Id)
        {
            var incompleteSpec = new GetDetailQuizzByID(Id);
            try
            {
                var quizz = await _repository.ListAsync<Quizz>(incompleteSpec);
                foreach (var item in quizz)
                {
                    var result = DeleteQuestion(item.Id);
                }
                await _repository.DeleteByIdAsync<Quizz>(Id);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<Guid> GetUserIdByCourseId(Guid Id)
        {
            return (await _repository.Find<Course>(item => item.Id == Id)
                .SingleOrDefaultAsync()).UserId;
        }

        
        public async Task UpdateCourse(Course request, IFormFile image, IFormFile trailer)
        {
            try
            {
                var existingItem = await _repository.GetByIdAsync<Course>(request.Id);

                existingItem.Title = request.Title;
                existingItem.Description = request.Description;
                existingItem.ImageUrl = request.ImageUrl;
                existingItem.TrailerUrl = request.TrailerUrl;
                existingItem.Status = request.Status;
                existingItem.Subtitle = request.Subtitle;
                existingItem.OriginPrice = request.OriginPrice;
                existingItem.UpdateAt = DateTime.Now;
                existingItem.PromotionId = request.PromotionId;
                existingItem.SubCategoryId = request.SubCategoryId;
                existingItem.LanguageId = request.LanguageId;
                await UpdateView(request.Id, image, trailer);

                await _repository.UpdateAsync(existingItem);
            }
            catch (Exception ex)
            {
                Result<Course>.Error(new[] { ex.Message });
            }
        }

        public async Task UpdateSection(Section request)
        {
            try
            {
                var existingItem = await _repository.GetByIdAsync<Section>(request.Id);

                existingItem.Title = request.Title;
                //existingItem.TotalTime = request.TotalTime;
                existingItem.UpdateAt = DateTime.Now;

                await _repository.UpdateAsync(existingItem);
            }
            catch (Exception ex)
            {
                Result<Section>.Error(new[] { ex.Message });
            }
        }

        public async Task<bool> UpdateLesson(LessonContent request)
        {
            try
            {
                var existingItem = await _repository.GetByIdAsync<Lesson>(request.Id);
                if(request.LessonTitle != "")
                {
                    existingItem.Title = request.LessonTitle;
                }
                if(request.Duration != 0)
                {
                    existingItem.Duration = request.Duration;
                }
                existingItem.UpdateAt = DateTime.Now;
               
                if(request.File != null)
                {
                    existingItem.VideoUrl = await UploadFile(request.File);
                }

                await _repository.UpdateAsync(existingItem);

                return true;
            }
            catch (Exception ex)
            {
                Result<Course>.Error(new[] { ex.Message });
                return false;
            }
        }

        public async Task<bool> GiveForReview(Guid id)
        {
            try
            {
                //find Course by Id
                Course course = await _repository.GetByIdAsync<Course>(id);

                //Check status must be "Waiting for approved"
                if (course.Status.Trim().ToLower() != "Draft".ToLower())
                    return false;

                //Updated Course's Status
                course.Status = "Waiting for approve";

                //Set updated date
                course.UpdateAt = DateTime.Now;

                //Update and SaveChange
                if (await _repository.UpdateAsync<Course>(course) == 1)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<bool>.Error(new[] { ex.Message });
            }
        }

        public async Task<List<Course>> GetCourseByUser<Course>(Guid Id)
        {
            var incompleteSpec = new GetCourseByUser(Id);
            try
            {
                var items = await _repository.ListAsync(incompleteSpec);

                return new List<Course>((IEnumerable<Course>)items);
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<List<Course>>.Error(new[] { ex.Message });
            }
        }

        public async Task<List<Course>> GetDraftCourse<Course>(Guid id)
        {
            var incompleteSpec = new GetDraftCourse(id);
            try
            {
                var items = await _repository.ListAsync(incompleteSpec);

                return new List<Course>((IEnumerable<Course>)items);
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<List<Course>>.Error(new[] { ex.Message });
            }
        }

        public async Task<List<Course>> GetUpcomingCourse<Course>(Guid id)
        {
            var incompleteSpec = new GetUpcomingCourse(id);
            try
            {
                var items = await _repository.ListAsync(incompleteSpec);

                return new List<Course>((IEnumerable<Course>)items);
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<List<Course>>.Error(new[] { ex.Message });
            }
        }
        
        public async Task<bool> DraftCourse(Guid id)
        {
            try
            {
                //find Course by Id
                Course course = await _repository.GetByIdAsync<Course>(id);

                //Check status must be "Waiting for approved"
                if (course.Status.Trim().ToLower() != "Waiting for approved".ToLower())
                    return false;

                //Updated Course's Status
                course.Status = "Draft";

                //Set updated date
                course.UpdateAt = DateTime.Now;

                //Update and SaveChange
                if (await _repository.UpdateAsync<Course>(course) == 1)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<bool>.Error(new[] { ex.Message });
            }
        }

        public async Task<List<OrderDetail>> GetTopCourse(Guid id)
        {
            var lstOrder = await _instructorRepository.GetInstructorByIdAsync<OrderDetail>(id)
                                                           .Include(c => c.Course)
                                                           .Where(c => c.Course.UserId.Equals(id))
                                                           .ToListAsync();
            return lstOrder;
        }

        public async Task<Section> GetSection(Guid id)
        {
            var incompleteSpec = new GetDetailSection(id);

            try
            {
                var items = await _repository.ListAsync<Section>(incompleteSpec);

                return items.First();
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<Section>.Error(new[] { ex.Message });
            }
        }
        
        public async Task<Lesson> GetLectureById(Guid id)
        {
            var incompleteSpec = new GetLessonById(id);

            try
            {
                var items = await _repository.ListAsync<Lesson>(incompleteSpec);

                return items.First();
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<Lesson>.Error(new[] { ex.Message });
            }
        }
        
        public async Task<List<Lesson>> GetLeture(Guid id)
        {
            var incompleteSpec = new GetDetailLesson(id);

            try
            {
                var items = await _repository.ListAsync(incompleteSpec);

                return new List<Lesson>((IEnumerable<Lesson>)items);
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<List<Lesson>>.Error(new[] { ex.Message });
            }
        }

        public async Task<List<Quizz>> GetQuizz(Guid SectionId)
        {
            var incompleteSpec = new GetDetailQuizz(SectionId);
            

            try
            {
                var items = await _repository.ListAsync(incompleteSpec);
                return new List<Quizz>((IEnumerable<Quizz>)items);
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<List<Quizz>>.Error(new[] { ex.Message });
            }
        }
        
        public async Task<List<Enrollment>> GetEnrollmentByUser(Guid UserId)
        {
            var incompleteSpec = new GetEnrollmentByUser(UserId);
            
            try
            {
                var items = await _repository.ListAsync(incompleteSpec);
                return new List<Enrollment>((IEnumerable<Enrollment>)items);
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<List<Enrollment>>.Error(new[] { ex.Message });
            }
        }

        public async Task<Course> GetCourseById(Guid CourseId)
        {
            var incompleteSpec = new GetCourseById(CourseId);

            try
            {
                var items = await _repository.ListAsync<Course>(incompleteSpec);

                return items.First();
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<Course>.Error(new[] { ex.Message });
            }
        }
        
        public async Task<Course> GetCommentById(Guid CourseId)
        {
            var incompleteSpec = new GetCommentById(CourseId);

            try
            {
                var items = await _repository.ListAsync<Course>(incompleteSpec);

                return items.First();
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<Course>.Error(new[] { ex.Message });
            }
        }

        public async Task<bool> AddQuizz(Quizz quizz)
        {
            try
            {
                await _repository.AddAsync<Quizz>(quizz);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<Guid> AddQuestion(QuizzQuestion quizz)
        {
            try
            {
                return (await _repository.AddAsync<QuizzQuestion>(quizz)).Id;
            }
            catch (Exception ex)
            {
                return Result<Guid>.Error(new[] { ex.Message });
            }
        }

        public async Task<Guid> AddAnswer(QuizzAnswer quizz)
        {
            try
            {
                return (await _repository.AddAsync<QuizzAnswer>(quizz)).Id;
            }
            catch (Exception ex)
            {
                return Result<Guid>.Error(new[] { ex.Message });
            }
        }

        public async Task<Quizz> GetQuizzById(Guid Id)
        {
            var incompleteSpec = new GetDetailQuizzByID(Id);

            try
            {
                var items = await _repository.ListAsync<Quizz>(incompleteSpec);

                return items.First();
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<Quizz>.Error(new[] { ex.Message });
            }
        }


        public async Task<QuizzQuestion> GetQuestionById(Guid Id)
        {
            var incompleteSpec = new GetDetailQuestionByID(Id);
            try
            {
                var items = await _repository.ListAsync<QuizzQuestion>(incompleteSpec);

                return items.First();
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<QuizzQuestion>.Error(new[] { ex.Message });
            }
        }

        public async Task<QuizzAnswer> GetAnswerById(Guid Id)
        {
            var incompleteSpec = new GetDetailAnswerByID(Id);

            try
            {
                var items = await _repository.ListAsync<QuizzAnswer>(incompleteSpec);

                return items.First();
            }
            catch (Exception ex)
            {
                // TODO: Log details here
                return Result<QuizzAnswer>.Error(new[] { ex.Message });
            }
        }

        public async Task<bool> UpdateQuizz(Quizz quizz)
        {
            try
            {
                await _repository.UpdateAsync<Quizz>(quizz);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> UpdateQuestion(QuizzQuestion quizz)
        {
            try
            {
                await _repository.UpdateAsync<QuizzQuestion>(quizz);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public async Task<bool> UpdateAnswer(QuizzAnswer quizz)
        {
            try
            {
                await _repository.UpdateAsync<QuizzAnswer>(quizz);
                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
