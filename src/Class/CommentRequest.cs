﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Endpoints.Courses
{
    public class CommentRequest
    {
        public Guid userId { get; set; }
        public Guid courseId { get; set; }
        public Guid? parentId { get; set; }
        public string comment { get; set; }
    }
}
