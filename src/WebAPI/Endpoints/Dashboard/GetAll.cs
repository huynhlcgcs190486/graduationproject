﻿using Ardalis.ApiEndpoints;
using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebAPI.Endpoints.Courses;

namespace WebAPI.Endpoints.Dashboard
{
    public class GetAll : BaseAsyncEndpoint<GetAllResponse>
    {
        private readonly ICourseService _courseService;
        public readonly IUserServices _userServices;
        private readonly ISubCategoryService _subCategoryService;
        private readonly ICategoryService _categoryService;
        private readonly ILanguageService _languageService;

        public GetAll(ICourseService courseService, IUserServices userServices, ICategoryService categoryService, ISubCategoryService subCategoryService, ILanguageService languageService)
        {
            _courseService = courseService;
            _userServices = userServices;
            _categoryService = categoryService;
            _subCategoryService = subCategoryService;
            _languageService = languageService;
        }

        [HttpPost("/GetAll")]
        [SwaggerOperation(
            Summary = "Gets a list of all Course",
            Description = "Gets a list of all Course",
            OperationId = "DashBoardGetAll.List",
            Tags = new[] { "DashboardEndpoints" })
        ]
        public override async Task<ActionResult<GetAllResponse>> HandleAsync(CancellationToken cancellationToken)
        {
            var courseList = await _courseService.GetAllCourse<Course>();
            var userList = await _userServices.GetAll();
            var languageList = await _languageService.GetAllLanguages();
            var categoryList = await _categoryService.GetAllCategory();
            var subcategoryList = await _subCategoryService.GetAllSubCategory();
            var result = new GetAllResponse
            {
                courses = courseList.Count(),
                courseWaiting = courseList.FindAll(item => item.Status == "Waiting for approve").Count(),
                courseBlock = courseList.FindAll(item => item.IsBlocked == true).Count(),
                courseDelete = courseList.FindAll(item => item.IsDeleted == true).Count(),
                admins = userList.FindAll(item => item.Role.Name == "Admin").Count(),
                students = userList.FindAll(item => item.Role.Name == "Student").Count(),
                instructors = userList.FindAll(item => item.Role.Name == "Instructor").Count(),
                language = languageList.Count(),
                category = categoryList.Count(),
                subcategory = subcategoryList.Count(),
            };
            return Ok(result);
        }
    }
}
