﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Endpoints.Dashboard
{
    public class GetAllResponse
    {
        public int courses { get; set; }
        public int students { get; set; }
        public int instructors { get; set; }
        public int admins { get; set; }
        public int courseWaiting { get; set; }
        public int courseBlock { get; set; }
        public int courseDelete { get; set; }
        public int category { get; set; }
        public int subcategory { get; set; }
        public int language { get; set; }
    }
    
     public class DashboardInstructorResponse
    {
        public int totalCourse { get; set; }
        public int totalStudent { get; set; }
        public int totalEnrollment { get; set; }
        public decimal totalSales { get; set; }
    }
}
