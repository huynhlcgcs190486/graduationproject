﻿using Ardalis.ApiEndpoints;
using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace WebAPI.Endpoints.Students
{
    public class Update: BaseAsyncEndpoint
    {
        private readonly IStudentService _studentService;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;
        public readonly RoleManager<Role> _roleManager;


        public Update(IStudentService studentService, UserManager<User> userManager, RoleManager<Role> roleManager, SignInManager<User> signInManager)
        {
            _studentService = studentService;
            this._signInManager = signInManager;
            this._userManager = userManager;
            this._roleManager = roleManager;

        }

        [HttpPut("/Student")]
        [SwaggerOperation(
            Summary = "Update Students",
            Description = "Update Students",
            OperationId = "Student.Update",
            Tags = new[] { "StudentEndpoints" })
        ]
        public async Task<ActionResult> UpdateAsync([FromBody]StudentRequest request, CancellationToken cancellationToken)
        {
            try
            {
                var user = _userManager.FindByIdAsync(request.Id.ToString());
                user.Result.UserName = request.UserName;
                user.Result.Email = request.Email;
                user.Result.FirstName = request.FirstName;
                user.Result.LastName = request.LastName;
                var oldRoleId = user.Result.RoleId;
                var role = _roleManager.FindByIdAsync(request.RoleId).Result;


                if (oldRoleId.ToString() != request.RoleId)
                {
                    user.Result.RoleId = Guid.Parse(request.RoleId);

                    await _userManager.RemoveFromRoleAsync(user.Result, role.Name);
                    await _userManager.AddToRoleAsync(user.Result, role.Name);
                }

                await _userManager.UpdateAsync(user.Result);

                return Ok();
            }
            catch(Exception e)
            {
                return Ok();
            }
            
        }
    }
}
