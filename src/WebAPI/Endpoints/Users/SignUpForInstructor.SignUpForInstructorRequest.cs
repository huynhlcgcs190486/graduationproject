﻿using Ardalis.ApiEndpoints;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using WebAPI.ApiModels;

namespace WebAPI.Endpoints.Users
{
    public class SignUpForInstructorRequest
    {
        [Required]
        public string UserName { set; get; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Please input again the same password")]
        public string ConfirmPassword { get; set; }
    }
}
