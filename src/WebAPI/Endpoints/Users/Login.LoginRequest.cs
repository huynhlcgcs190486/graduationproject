﻿

using System.ComponentModel.DataAnnotations;

namespace WebAPI.Endpoints.Users
{
    public class LoginRequest
    {
        public string Email { get; set; }

        public string Password { get; set; }
    }
}
