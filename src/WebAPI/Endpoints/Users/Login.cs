﻿using Ardalis.ApiEndpoints;
using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Threading;
using System.Threading.Tasks;
using WebAPI.ApiModels;

namespace WebAPI.Endpoints.Users
{
    public class Login : BaseAsyncEndpoint<LoginRequest, UserResponse>
    {
        private readonly IUserServices _userservice;
        private readonly SignInManager<User> _signInManager;
        private readonly UserManager<User> _userManager;

        public Login(IUserServices userService, SignInManager<User> signInManager, UserManager<User> userManager)
        {
            _userservice = userService;
            _signInManager = signInManager;
            _userManager = userManager;
        }

        [HttpPost("/Login")]
        [SwaggerOperation(
            Summary = "Login with authentication",
            Description = "Login with authentication",
            OperationId = "User.Login",
            Tags = new[] { "UserEndpoints" })
        ]
        public override async Task<ActionResult<UserResponse>> HandleAsync([FromBody] LoginRequest loginRequest, CancellationToken cancellationToken)
        {
            //var user = await _userservice.Login(loginRequest.Email, loginRequest.Password);

            //if (user != null)
            //{
            //    var token = await _userservice.GenerateJSONWebToken(user, HttpContext);
            //    return Ok(token);
            //}
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByNameAsync(loginRequest.Email);
                if (await _userManager.CheckPasswordAsync(user, loginRequest.Password) == false)
                {
                    ModelState.AddModelError("message", "Invalid credentials");
                }

                var result = await _signInManager.PasswordSignInAsync(loginRequest.Email, loginRequest.Password, false, false);

                if (result.Succeeded)
                {
                    if (user != null)
                    {
                        var token = await _userservice.GenerateJSONWebToken(user, HttpContext);
                        return Ok(token);
                    }
                }

            }
            return Unauthorized();
        }

        //[HttpPost("/Logout")]
        //[SwaggerOperation(
        //    Summary = "Logout with authentication",
        //    Description = "Logout with authentication",
        //    OperationId = "User.Logout",
        //    Tags = new[] { "UserEndpoints" })
        //]
        //public async Task<ActionResult> LogoutAsync(CancellationToken cancellationToken)
        //{
        //    await _signInManager.SignOutAsync();

        //    return Unauthorized();
        //}
    }
}
