﻿using Ardalis.ApiEndpoints;
using Ardalis.Result;
using Core.Entities;
using Core.Interfaces;
using Web.Endpoints.ToDoItems;
using WebAPI.ApiModels;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace WebAPI.Endpoints.Users
{
    public class Create : BaseAsyncEndpoint<NewUserRequest, UserResponse>
    {
        public readonly IUserServices _userServices;
        public readonly UserManager<User> _userManager;
        public readonly RoleManager<Role> _roleManager;
        public readonly SignInManager<User> _signInManager;
        public Create(IUserServices userServices, UserManager<User> userManager, RoleManager<Role> roleManager, SignInManager<User> signInManager)
        {
            this._userServices = userServices;
            this._userManager = userManager;
            this._roleManager = roleManager;
            this._signInManager = signInManager;
        }
        [HttpPost("/Users")]
        [SwaggerOperation(
            Summary = "Create a new User",
            Description = "Create a new User",
            OperationId = "User.Create",
            Tags = new[] { "UserEndpoints" })
         ]
        public override async Task<ActionResult<UserResponse>> HandleAsync([FromBody] NewUserRequest request, CancellationToken cancellationToken = default)
        {
            if (_userServices.IsUserNameExisted(request.UserName) == true)
            {
                return BadRequest("UserName is Already existed");
            }   

            if(_userServices.IsEmailExisted(request.Email) == true)
            {
                return BadRequest("Email is Already existed");
            }

            var role = _roleManager.FindByIdAsync(request.RoleId).Result;
            var user = new User
            {
                UserName = request.UserName,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Status = true,
                IsDeleted = false,
                IsStatus = true,
                RoleId = role.Id,
            };

            try
            {
                var userAdd = await _userManager.CreateAsync(user, request.Password);
                if (userAdd.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, role.Name);
                    await _signInManager.SignInAsync(user, isPersistent: false);
                }
            }
            catch (Exception e)
            {
                return Result<ActionResult<UserResponse>>.Error(new[] { e.Message });

            }

            var createUser = new UserResponse
            {
                Id = _userServices.GetUserIdJustAdded(request.UserName),
                UserName = request.UserName,
                RoleName =  request.RoleId
            };
            return Ok(createUser);
        }
    }
}
