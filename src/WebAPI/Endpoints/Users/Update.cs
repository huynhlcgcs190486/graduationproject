﻿using Ardalis.ApiEndpoints;
using Ardalis.Result;
using Core.Interfaces;
using WebAPI.ApiModels;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Core.Entities;
using Microsoft.AspNetCore.Identity;

namespace WebAPI.Endpoints.Users
{
    public class Update : BaseAsyncEndpoint<UpdateUserRequest, UserResponse>
    {
        private readonly IUserServices _userServices;
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;

        public Update(IUserServices userServices, UserManager<User> userManager, SignInManager<User> signInManager)
        {
            this._userServices = userServices;
            this._signInManager = signInManager;
            this._userManager = userManager;
        }

        [HttpPut("/Users")]
        [SwaggerOperation(
            Summary = "Updates an User",
            Description = "Updates an User",
            OperationId = "User.Update",
            Tags = new[] { "UserEndpoints" })
        ]
        public override async Task<ActionResult<UserResponse>> HandleAsync([FromBody]UpdateUserRequest request, CancellationToken cancellationToken = default)
        {
            if (_userServices.IsUserNameExisted(request.UserName) == true)
            {
                return BadRequest("UserName is Already existed");
            }

            var existingUser = await _userServices.GetById(request.Id);

            if (existingUser == null)
            {
                return NotFound();
            }

            var user = _userManager.FindByIdAsync(request.Id.ToString());
            user.Result.UserName = request.UserName;
            user.Result.Email = request.Email;
            await _userManager.UpdateAsync(user.Result);
    

            var response = new UserResponse
            {
                Id = existingUser.Id,
                UserName = existingUser.UserName,
                RoleName = _userServices.GetRoleNameByRoleId(existingUser.RoleId)
            };

            return Ok(response);
        }
        
        [HttpPut("/Users/Password")]
        [SwaggerOperation(
            Summary = "Updates an User",
            Description = "Updates an User",
            OperationId = "User.Update",
            Tags = new[] { "UserEndpoints" })
        ]
        public async Task<ActionResult<UserResponse>> UpdatePasswordAsync([FromBody] UpdateUserPasswordRequest request, CancellationToken cancellationToken = default)
        {

            var existingUser = await _userServices.GetById(request.Id);

            if (existingUser == null)
            {
                return NotFound();
            }

            var user = _userManager.FindByIdAsync(request.Id.ToString());
            var isLogin = await _signInManager.PasswordSignInAsync(user.Result.UserName, request.OldPassword, false, false);

            if (isLogin.Succeeded && request.NewPassword == request.Confirm)
            {
                user.Result.PasswordHash = _userManager.PasswordHasher.HashPassword(user.Result, request.NewPassword);
                await _userManager.UpdateAsync(user.Result);
            }

            var response = new UserResponse
            {
                Id = existingUser.Id,
                UserName = existingUser.UserName,
                RoleName = _userServices.GetRoleNameByRoleId(existingUser.RoleId)
            };

            return Ok(response);
        }
    }
}
