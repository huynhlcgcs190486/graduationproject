﻿using Core.Entities;
using Core.Mapper.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Endpoints.Users
{
    public class JWTResponse
    {
        public string Token { get; set; }
        public AccountModel User { get; set; }
    }
}
