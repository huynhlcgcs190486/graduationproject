﻿using Ardalis.ApiEndpoints;
using Ardalis.Result;
using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using WebAPI.ApiModels;

namespace WebAPI.Endpoints.Users
{
    public class SignUpForInstructor : BaseAsyncEndpoint<SignUpForInstructorRequest, UserResponse>
    {
        public readonly IUserServices _userServices;
        private readonly UserManager<User> _userManager;
        public readonly RoleManager<Role> _roleManager;
        public readonly SignInManager<User> _signInManager;
        public SignUpForInstructor(IUserServices userServices, UserManager<User> userManager, RoleManager<Role> roleManager, SignInManager<User> signInManager)
        {
            this._userServices = userServices;
            this._userManager = userManager;
            this._roleManager = roleManager;
            this._signInManager = signInManager;

        }
        [HttpPost("/Users/SignUpForInstructor")]
        [SwaggerOperation(
            Summary = "Sign Up Your New Account",
            Description = "Sign Up A New User",
            OperationId = "User.SignUpForInstructor",
            Tags = new[] { "UserEndpoints" })
         ]
        public override async Task<ActionResult<UserResponse>> HandleAsync([FromBody] SignUpForInstructorRequest request, CancellationToken cancellationToken = default)
        {
            //if (_userServices.IsUserNameExisted(request.UserName) == true)
            //{
            //    HttpContext.Response.StatusCode = 302;
            //    return Ok("UserName is Already existed");
            //}

            //if (_userServices.IsEmailExisted(request.Email) == true)
            //{
            //    HttpContext.Response.StatusCode = 302;
            //    return Ok("Email is Already existed");
            //}

            var role = _roleManager.FindByIdAsync("2474077b-6efb-42e7-79ab-08da9e12f4f5").Result;
            var user = new User
            {
                UserName = request.UserName,
                Email = request.Email,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Status = true,
                IsDeleted = false,
                IsStatus = true,
                RoleId = role.Id,
            };

            try
            {
                var userAdd = await _userManager.CreateAsync(user, request.Password);
                if (userAdd.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, role.Name);
                    await _signInManager.SignInAsync(user, isPersistent: false);
                }
            }
            catch (Exception e)
            {
                return Result<ActionResult<UserResponse>>.Error(new[] { e.Message });

            }

            var createUser = new UserResponse
            {
                Id = _userServices.GetUserIdJustAdded(request.UserName),
                UserName = request.UserName,
                Email = request.Email,
                FirstName = request.UserName,
                LastName = request.LastName,

            };
            return Ok(createUser);
        }
    }
}
