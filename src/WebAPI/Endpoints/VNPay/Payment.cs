﻿using Ardalis.ApiEndpoints;
using Class;
using Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Protocols;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Configuration;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Twilio.TwiML.Voice;

namespace WebAPI.Endpoints.VNPay
{
    public class Payment : BaseAsyncEndpoint
    {
        private readonly ICourseService _courseService;
        private readonly IOrderDetailService _orderDetailService;
        public Payment(ICourseService courseService, IOrderDetailService orderDetailService)
        {
            _courseService = courseService;
            _orderDetailService = orderDetailService;
        }

        [HttpPost("/Payment")]
        [SwaggerOperation(
           Summary = "Checkout with Vnpay",
           Description = "Gets a list of all Payout",
           OperationId = "VnPay.Payment",
           Tags = new[] { "VNPayEndpoints" })
       ]
        public async Task<ActionResult<PaymentResponse>> PaymentAsync ([FromBody]PaymentResponse data, CancellationToken cancellationToken = default)
        {
            string url = "http://sandbox.vnpayment.vn/paymentv2/vpcpay.html";
            string returnUrl = "http://localhost:57678/PayConfirm/";
            string tmnCode = "GHHNT2HB";
            string hashSecret = "BAGAOHAPRHKQZASKQZASVPRSAKPXNYXS";

            PayLib pay = new PayLib();

            pay.AddRequestData("vnp_Version", "2.0.0"); //Phiên bản api mà merchant kết nối. Phiên bản hiện tại là 2.0.0
            pay.AddRequestData("vnp_Command", "pay"); //Mã API sử dụng, mã cho giao dịch thanh toán là 'pay'
            pay.AddRequestData("vnp_TmnCode", tmnCode); //Mã website của merchant trên hệ thống của VNPAY (khi đăng ký tài khoản sẽ có trong mail VNPAY gửi về)
            pay.AddRequestData("vnp_Amount", data.amount); //số tiền cần thanh toán, công thức: số tiền * 100 - ví dụ 10.000 (mười nghìn đồng) --> 1000000
            pay.AddRequestData("vnp_BankCode", ""); //Mã Ngân hàng thanh toán (tham khảo: https://sandbox.vnpayment.vn/apis/danh-sach-ngan-hang/), có thể để trống, người dùng có thể chọn trên cổng thanh toán VNPAY
            pay.AddRequestData("vnp_CreateDate", DateTime.Now.ToString("yyyyMMddHHmmss")); //ngày thanh toán theo định dạng yyyyMMddHHmmss
            pay.AddRequestData("vnp_CurrCode", "VND"); //Đơn vị tiền tệ sử dụng thanh toán. Hiện tại chỉ hỗ trợ VND
            pay.AddRequestData("vnp_IpAddr", Util.GetIpAddress()); //Địa chỉ IP của khách hàng thực hiện giao dịch
            pay.AddRequestData("vnp_Locale", "vn"); //Ngôn ngữ giao diện hiển thị - Tiếng Việt (vn), Tiếng Anh (en)
            pay.AddRequestData("vnp_OrderInfo", data.courseId.ToString()); //Thông tin mô tả nội dung thanh toán
            pay.AddRequestData("vnp_OrderType", "other"); //topup: Nạp tiền điện thoại - billpayment: Thanh toán hóa đơn - fashion: Thời trang - other: Thanh toán trực tuyến
            pay.AddRequestData("vnp_ReturnUrl", returnUrl); //URL thông báo kết quả giao dịch khi Khách hàng kết thúc thanh toán
            pay.AddRequestData("vnp_TxnRef", DateTime.Now.Ticks.ToString()); //mã hóa đơn

            var item = new PaymentResponse()
            {
                Url = pay.CreateRequestUrl(url, hashSecret),
            };

            return Ok(item);
        }

        [HttpGet("/PayConfirm")]
        [SwaggerOperation(
           Summary = "PayConfirm payment",
           Description = "PayConfirm payment",
           OperationId = "VnPay.PayConfirm",
           Tags = new[] { "VNPayEndpoints" })
       ]
        public async Task<ActionResult> PayConfirmAsync([FromQuery] VNPayResponse Request, CancellationToken cancellationToken = default)
        {
            string hashSecret = "BAGAOHAPRHKQZASKQZASVPRSAKPXNYXS";
            if (Request != null)
            {
                PayLib payLib = new PayLib();
                long orderId = Convert.ToInt64(Request.vnp_TxnRef); //mã hóa đơn
                long vnpayTranId = Convert.ToInt64(Request.vnp_TransactionNo); //mã giao dịch tại hệ thống VNPAY
                string vnp_ResponseCode = Request.vnp_ResponseCode; //response code: 00 - thành công, khác 00 - xem thêm https://sandbox.vnpayment.vn/apis/docs/bang-ma-loi/
                string vnp_SecureHash = Request.vnp_SecureHash; //hash của dữ liệu trả về
                string vnp_OrderInfo = Request.vnp_OrderInfo;
                string vnp_Amount = Request.vnp_Amount;
                if (vnp_ResponseCode == "00")
                {
                    var userID = GetLoggedUserId(); 

                    //var userID = Guid.Parse("2615c528-bc7c-46a5-4415-08da9e198bcb");
                    await _courseService.CreateCourseEnrollment(Guid.Parse(vnp_OrderInfo), userID);
                    await _orderDetailService.AddOrder(userID, Guid.Parse(vnp_OrderInfo), decimal.Parse(vnp_Amount));
                    return RedirectToAction("https://localhost:44374/Course/CourseDetail/"+ vnp_OrderInfo);
                }
                else
                {
                    return RedirectToAction("https://localhost:44374/Home/Index"); ;
                }
            }

            return Ok();
        }
        private Guid GetLoggedUserId()
        {
            if (!User.Identity.IsAuthenticated)
                throw new System.Security.Authentication.AuthenticationException();

            string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;

            return Guid.Parse(userId);
        }
    }
}
