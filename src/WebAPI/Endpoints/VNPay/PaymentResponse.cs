﻿using System;

namespace WebAPI.Endpoints.VNPay
{
    public class PaymentResponse
    {
        public string Url { get; set; }
        public string amount { get; set; }
        public string courseId { get; set; }
    }
}
