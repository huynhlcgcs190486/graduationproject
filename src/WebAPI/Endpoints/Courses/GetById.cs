﻿using Ardalis.ApiEndpoints;
using Class;
using Core.Entities;
using Core.Interfaces;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;

namespace WebAPI.Endpoints.Courses
{
    public class GetById : BaseAsyncEndpoint
    {
        private readonly ICourseService _courseService;

        public GetById(ICourseService courseService)
        {
            _courseService = courseService;
        }

        [HttpGet("/Course/{id}")]
        [SwaggerOperation(
            Summary = "Gets a single Course",
            Description = "Gets a single Course by Id",
            OperationId = "Course.GetById",
            Tags = new[] { "CourseEndpoints" })
        ]
        public async Task<ActionResult<Course>> GetCourseByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            var item = (await _courseService.GetCourseById(id));

            var response = new CourseResponse
            {
                Id = item.Id,
                Price = item.OriginPrice,
                Title = item.Title,
                Image = item.ImageUrl,
                Sections = item.Sections.Select(item => new CourseSectionResponse {
                    CourseId = item.CourseId,
                    CourseContentTitle = item.Title,
                    Lessons = item.Lessons.Select(items => new CourseLectureResponse { 
                        Id = items.Id.ToString(),
                        Title = items.Title,
                        Duration = items.Duration,
                        VideoUrl = items.VideoUrl,
                        SectionId = items.SectionId.ToString(),
                    }),
                    Quizzs = item.Quizzes.Select(items => new QuizzResponse {
                        Id = items.Id,
                        Title = items.Title,
                        IsDelete = items.IsDeleted,
                        Question = items.QuizzQuestions.Select(item => new QuestionResponse
                        {
                            Id = item.Id,
                            Title = item.Title,
                            QuizzId = item.QuizzId,
                            Answer = item.QuizzAnswers.Select(item => new AnswerResponse
                            {
                                Id = item.Id,
                                Title = item.Content,
                                QuestionId = item.QuestionId,
                            }),
                        }),
                    })
                }),
            };
            return Ok(response);
        }

        [HttpGet("/Course/Comment/{id}")]
        [SwaggerOperation(
            Summary = "Gets a single Course",
            Description = "Gets a single Course by Id",
            OperationId = "Course.GetCommentById",
            Tags = new[] { "CourseEndpoints" })
        ]
        public async Task<ActionResult<Course>> GetCommentByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            var item = (await _courseService.GetCommentById(id));

            var response = new CommentResponse
            {
                Id = item.Id,
                Discussions = item.Discussions.Select(item => new CourseCommentResponse
                {
                    Id = item.Id,
                    CourseId = item.CourseId,
                    UserId = item.UserId,
                    parentId = item.parentId,
                    Comment = item.Comment,
                    Username = item.User.UserName,
                }),
            };
            return Ok(response);
        }


        [HttpGet("/CourseStudent/{id}")]
        [SwaggerOperation(
            Summary = "Gets a single Course",
            Description = "Gets a single Course by Id",
            OperationId = "Course.GetCourseById",
            Tags = new[] { "CourseEndpoints" })
        ]
        public async Task<ActionResult<CourseStudentResponse>> GetCourseByIdStudentAsync(Guid id, CancellationToken cancellationToken)
        {
            var item = (await _courseService.GetDetailCourse(id));
            var response = new CourseStudentResponse
            {
                Id = item.Id,
                Price = item.OriginPrice,
                Title = item.Title,
                Image = item.ImageUrl,
                Description = item.Description,
                Section = item.Sections,
            };
            return Ok(response);
        }
        
        [HttpGet("/Course/Lectures/{id}")]
        [SwaggerOperation(
            Summary = "Gets a single Course",
            Description = "Gets a single Course by Id",
            OperationId = "Course.GetCourseById",
            Tags = new[] { "CourseEndpoints" })
        ]
        public async Task<ActionResult<LessonContent>> GetLecturesBySectionAsync(Guid id, CancellationToken cancellationToken)
        {
            var items = (await _courseService.GetLeture(id));

            var result = items.Select(item => new LessonContent
            {
                Id = item.Id,
                LessonTitle = item.Title,
                Duration = (float)item.Duration,
                Video = item.VideoUrl,
            });

            return Ok(result);
        }

        [HttpGet("/Course/Enrollment")]
        [SwaggerOperation(
            Summary = "Gets a single Course",
            Description = "Gets a single Course by Id",
            OperationId = "Course.GetCourseById",
            Tags = new[] { "CourseEndpoints" })
        ]
        public async Task<ActionResult<EnrollmentCourseResponse>> GetEnrollmentByUserAsync(CancellationToken cancellationToken)
        {
            var userID = GetLoggedUserId();

            //var userID = Guid.Parse("2615c528-bc7c-46a5-4415-08da9e198bcb");
            var items = (await _courseService.GetEnrollmentByUser(userID));

            var result = items.Select(item => new EnrollmentCourseResponse
            {
                Id = item.Id,
                CourseId = item.CourseId.ToString(),
            });

            return Ok(result);
        }

        [HttpGet("/Course/Lecture/{id}")]
        [SwaggerOperation(
            Summary = "Gets a single Course",
            Description = "Gets a single Course by Id",
            OperationId = "Course.GetCourseById",
            Tags = new[] { "CourseEndpoints" })
        ]
        public async Task<ActionResult<LessonContentResponse>> GetLecturesByIdAsync(Guid id, CancellationToken cancellationToken)
        {
            var item = await _courseService.GetLectureById(id);

            var result = new LessonContentResponse
            {
                Id = item.Id,
                LessonTitle = item.Title,
                Duration = (float)item.Duration,
                Video = item.VideoUrl,
            };

            return Ok(result);
        }

        [HttpGet("/Course/Quizz/{id}")]
        [SwaggerOperation(
            Summary = "Gets a single Course",
            Description = "Gets a single Course by Id",
            OperationId = "Course.GetCourseById",
            Tags = new[] { "CourseEndpoints" })
        ]
        public async Task<ActionResult<QuizzResponse>> GetQuizzBySectionAsync(Guid id, CancellationToken cancellationToken)
        {
            var items = (await _courseService.GetQuizz(id));

            var result = items.Select(item => new QuizzResponse
            {
                Id = item.Id,
                Title = item.Title,
                IsDelete = item.IsDeleted,
                Question = item.QuizzQuestions.Select(item => new QuestionResponse
                {
                    Id = item.Id,
                    Title = item.Title,
                    QuizzId = item.QuizzId,
                    Answer = item.QuizzAnswers.Select(item => new AnswerResponse 
                    {
                        Id = item.Id,
                        Title = item.Content,
                        QuestionId = item.QuestionId,
                        isCorrect = item.IsCorrect,
                    }),
                }),
            });

            return Ok(result);
        }

        [HttpGet("/Course/TopCourse")]
        [SwaggerOperation(
            Summary = "Gets a list of Top Course by Instructor ID",
            Description = "Gets a list of all Course",
            OperationId = "Course.TopCourse",
            Tags = new[] { "CourseEndpoints" })
        ]
        public async Task<ActionResult<List<CourseResponse>>> GetTopCourseAsync(CancellationToken cancellationToken)
        {
            var userId = GetLoggedUserId();
            var listOrder = await _courseService.GetTopCourse(userId);
            List<TopCourse> result = (from p in listOrder
                                      group p.Price by p.CourseId into g
                                      select new TopCourse { id = g.Key, price = g.Sum() }).OrderByDescending(x => x.price).ToList();

            var item = (await _courseService.GetDetailCourse(result[0].id));
            var response = new CourseResponse
            {
                Id = item.Id,
                Price = item.OriginPrice,
                Title = item.Title,
                //Section = item.Sections,
            };
            return Ok(response);
        }

        private Guid GetLoggedUserId()
        {
            if (!User.Identity.IsAuthenticated)
                throw new System.Security.Authentication.AuthenticationException();

            string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;

            return Guid.Parse(userId);
        }
    }
}