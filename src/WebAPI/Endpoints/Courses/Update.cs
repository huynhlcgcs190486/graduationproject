﻿using System.Threading;
using Ardalis.ApiEndpoints;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Threading.Tasks;
using System;
using Core.Interfaces;
using System.Linq;
using System.Security.Claims;
using Microsoft.AspNetCore.Http;
using WebAPI.Endpoints.Courses;

namespace Web.Endpoints.Courses
{
    public class Update : BaseAsyncEndpoint
    {
        private readonly ICourseService _courseService;

        public Update(ICourseService courseService)
        {
            _courseService = courseService;
        }

        [HttpPut("/Courses/Reject/{id}")]
        [SwaggerOperation(
            Summary = "Updates a Course",
            Description = "Updates a Course with status as Draff",
            OperationId = "Course.Update",
            Tags = new[] { "CourseEndpoints" })
        ]
        public async Task<bool> RejectedAsync(Guid id, CancellationToken cancellationToken)
        {
            return await _courseService.RejectedCourse(id);
        }
        
        [HttpPut("/Courses/BestSeller")]
        [SwaggerOperation(
            Summary = "Updates a Course",
            Description = "Updates a Course with IsBestSeller as true or false",
            OperationId = "Course.Update",
            Tags = new[] { "CourseEndpoints" })
        ]
        public async Task<bool> BestSellerAsync(Guid id, CancellationToken cancellationToken)
        {
            return await _courseService.BestSeller(id);
        }
        
        [HttpPut("/Courses/Block/{id}")]
        [SwaggerOperation(
            Summary = "Updates a Course",
            Description = "Updates a Course with status as blocked or last status",
            OperationId = "Course.Update",
            Tags = new[] { "CourseEndpoints" })
        ]
        public async Task<bool> BlockedCourseAsync(Guid id, CancellationToken cancellationToken)
        {
            return await _courseService.BlockedCourse(id);
        }
        
        [HttpPut("/Courses/Active/{id}")]
        [SwaggerOperation(
            Summary = "Updates a Course",
            Description = "Updates a Course with status as approval",
            OperationId = "Course.Update",
            Tags = new[] { "CourseEndpoints" })
        ]
        public async Task<bool> ApprovedAsync(Guid id, CancellationToken cancellationToken)
        {
            return await _courseService.ApprovedCourse(id);
        }

        private Guid GetLoggedUserId()
        {
            if (!User.Identity.IsAuthenticated)
                throw new System.Security.Authentication.AuthenticationException();

            string userId = User.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;

            return Guid.Parse(userId);
        }
    }
}
