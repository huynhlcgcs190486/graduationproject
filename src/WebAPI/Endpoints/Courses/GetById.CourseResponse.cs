﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Endpoints.Courses
{
    public class CourseResponse
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string Image { get; set; }
        public IEnumerable<CourseSectionResponse> Sections { get; set; }
    }

    public class CommentResponse
    {
        public Guid Id { get; set; }
        public IEnumerable<CourseCommentResponse> Discussions { get; set; }
    }
    
    public class CourseCommentResponse
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid CourseId { get; set; }
        public string Comment { get; set; }
        public string Username { get; set; }
        public Guid? parentId { get; set; }
    }

    public class CourseStudentResponse
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string Image { get; set; }
        public string Description { get; set; }
        public List<Quizz> Quizzs { get; set; }
        public ICollection<Section> Section { get; set; }
    }
}

