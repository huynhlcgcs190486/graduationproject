﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Endpoints.Courses
{
    public class EnrollmentCourseRequest
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }

        public Guid CourseId { get; set; }

        public DateTime CreateAt { get; set; }

        public DateTime? UpdateAt { get; set; }

        public bool IsDeleted { get; set; }

    }
}
