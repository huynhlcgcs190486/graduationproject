﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebAPI.Endpoints.Courses
{
    public class CourseSectionResponse
    {
        public Guid CourseId { get; set; }
        public string CourseContentTitle { get; set; }
        public IEnumerable<CourseLectureResponse> Lessons { get; set; }
        public IEnumerable<QuizzResponse> Quizzs { get; set; }
    }

    public class QuizzRequest
    {
        public Guid id { get; set; }
        public string title { get; set; }
    }

    public class QuestionRequest
    {
        public Guid id { get; set; }
        public string htmlContent { get; set; }
        public string title { get; set; }
    }

    public class AnswerRequest
    {
        public Guid id { get; set; }
        public string answer { get; set; }
        public string description { get; set; }
    }
}
