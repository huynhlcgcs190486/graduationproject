﻿using Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Endpoints.Courses
{
    public class ListResponse
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string Category { get; set; }
        public string Status { get; set; }
        public string? UpdateAt { get; set; }
        public string Image { get; set; }
        public string Video { get; set; }
    }
    public class SearchCourseResponse
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public string Category { get; set; }
        public string Username { get; set; }
        public string? UpdateAt { get; set; }
        public string Image { get; set; }
        public string Video { get; set; }
    }

    public class SearchRequest
    {
        public string key { get; set; }
    }
    
    public class FilterRequest
    {
        public Guid[]? Category { get; set; }
        public int? Min { get; set; } = 0;
        public int? Max { get; set; } = 100000;
    }

    public class TopCourse
    {
        public Guid id { get; set; }
        public decimal price { get; set; }
    }

}
