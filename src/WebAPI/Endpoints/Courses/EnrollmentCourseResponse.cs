﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Endpoints.Courses
{
    public class EnrollmentCourseResponse
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }

        public string CourseId { get; set; }

        public DateTime CreateAt { get; set; }

        public DateTime? UpdateAt { get; set; }
    }
}
