﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Endpoints.DetailStudent
{
    public class EnrollmentResponse
    {
        public Guid Id { get; set; }
        public Guid CourseId { get; set; }

        public string Title { get; set; }

        public string InstructorName { get; set; }

        public Decimal Price { get; set; }

        public DateTime DateBuy { get; set; }
        public string Image { get; set; }
    }
}
