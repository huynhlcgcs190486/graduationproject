﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GUI.Models
{
    public class JWTResponse
    {
        public string Token { get; set; }
        public AccountModel User { get; set; }
    }
}
