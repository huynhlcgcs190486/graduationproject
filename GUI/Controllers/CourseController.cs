﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GUI.Controllers
{
    public class CourseController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult CourseWaiting()
        {
            return View();
        }

        public IActionResult CourseInstructor()
        {
            ViewBag.token = User.Claims.ElementAt(8).Value;
            return View();
        }

        public IActionResult Create()
        {
            ViewBag.token = User.Claims.ElementAt(8).Value;
            return View();
        }

        public IActionResult CourseStudent()
        {
            return View();
        }

        public IActionResult Enrollment()
        {
            ViewBag.studentId = User.Claims.ElementAt(4).Value;
            return View();
        }

        public IActionResult CourseDetail(Guid id)
        {
            ViewBag.token = User.Claims.ElementAt(8).Value;
            ViewBag.courseId = id;
            return View();
        }

        public IActionResult Category(Guid id)
        {
            ViewBag.cateId = id;
            return View();
        }

        public IActionResult CourseEnrolled(string id)
        {
            ViewBag.url = "https://localhost:44374/Course/CourseEnrolled/" + id;
            ViewBag.courseId = id;
            ViewBag.studentId = User.Claims.ElementAt(4).Value;
            return View();
        }
        
        public IActionResult FilterCourse()
        {
            return View();
        }
    }
}
