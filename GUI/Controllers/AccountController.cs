﻿using Core.Interfaces;
using GUI.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace GUI.Controllers
{
    public class AccountController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public AccountController(IConfiguration configuration, IHttpContextAccessor httpContextAccessor)
        {
            _configuration = configuration;
            _httpContextAccessor = httpContextAccessor;
        }

        public IActionResult Index()
        {
            if (User.Claims != null && User.Claims.ToArray().Length >= 8 && User.Claims.ElementAt(2).Value == "Student")
            {

                return RedirectToAction("Index", "Home");
            }
            else if (User.Claims != null && User.Claims.ToArray().Length >= 8 && User.Claims.ElementAt(2).Value == "Instructor")
            {
                return RedirectToAction("Dashboard", "Instructor");

            }
            else if (User.Claims != null && User.Claims.ToArray().Length >= 8 && User.Claims.ElementAt(2).Value == "Admin")
            {
                return RedirectToAction("Index", "AdminHome");

            }
            else
            {
                return RedirectToAction("Login","Account");

            }
        }
        [HttpGet]
        public async Task<ActionResult> Login()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login(string email, string password)
        {
            var request = new LoginModel();
            request.email = email;
            request.password = password;
            var json = JsonConvert.SerializeObject(request);
            string apiUrl = "http://localhost:57678/Login";
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");
            using (HttpClient client = new HttpClient())
            {
                HttpResponseMessage response = await client.PostAsync(apiUrl, httpContent);
                if (response.IsSuccessStatusCode)
                {
                    var body = await response.Content.ReadAsStringAsync();
                    IdentityModelEventSource.ShowPII = true;
                    TokenValidationParameters validationParameters = new TokenValidationParameters();
                    var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:Key"]));

                    var principal = new JwtSecurityTokenHandler().ValidateToken(body, new TokenValidationParameters()
                    {
                        ValidateIssuerSigningKey = true,
                        ValidateIssuer = false,
                        ValidateAudience = false,
                        IssuerSigningKey = securityKey,
                        ValidIssuer = _configuration["Jwt:Issuer"],
                        ValidAudience = _configuration["Jwt:Issuer"],
                    }, out SecurityToken validatedToken);

                    var claimIdentity = new ClaimsIdentity();
                    claimIdentity.AddClaim(new Claim(ClaimTypes.Authentication, body));

                    principal.AddIdentity(claimIdentity);
                    var token = new AuthenticationProperties()
                    {
                        IsPersistent = true,
                        ExpiresUtc = DateTimeOffset.UtcNow.AddMinutes(10),
                    };

                    //SignInAsync is a Extension method for Sign in a principal for the specified scheme.
                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal, token);
                }
            }
            return RedirectToAction("Index","Account");
        }
        public IActionResult Register()
        {
            return View();
        }
        public IActionResult RegisterForInstructor()
        {
            return View();
        }

        public IActionResult Logout()
        {
            //read cookie from IHttpContextAccessor  
            string cookieValueFromContext = _httpContextAccessor.HttpContext.Request.Cookies[".AspNetCore.Cookies"];
            Response.Cookies.Delete(".AspNetCore.Cookies");
            return RedirectToAction("Index","Home");
        }
    }
}
